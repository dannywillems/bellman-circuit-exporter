## Bellman circuit exporter

Write your circuit with [bellman](https://github.com/zkcrypto/bellman) and
export it in
[J-R1CS](https://docs.zkproof.org/pages/standards/accepted-workshop2/proposal--zk-interop-jr1cs.pdf)

### Example

```rust
use bellman::{Circuit, ConstraintSystem, SynthesisError};

use bellman_circuit_exporter;
use bls12_381;

pub struct MyCircuit {
    x: Option<bls12_381::Scalar>,
}

impl Circuit<bls12_381::Scalar> for MyCircuit {
    fn synthesize<CS: ConstraintSystem<bls12_381::Scalar>>(
        self,
        cs: &mut CS,
    ) -> Result<(), SynthesisError> {
        let x = cs.alloc(|| "x", || self.x.ok_or(SynthesisError::AssignmentMissing))?;
        let x_square_val = self.x.map(|e| {
            e.square();
            e
        });
        let x_square = cs.alloc_input(
            || "x_square",
            || x_square_val.ok_or(SynthesisError::AssignmentMissing),
        )?;
        // Enforce: x * x = x_square
        cs.enforce(|| "x_square", |lc| lc + x, |lc| lc + x, |lc| lc + x_square);
        Ok(())
    }
}

pub fn main() {
    let circuit = MyCircuit {
        x: Some(bls12_381::Scalar::one()),
    };
    bellman_circuit_exporter::export_to_json::<MyCircuit, bls12_381::Scalar>(
        circuit,
        String::from("simple.r1cs"),
    )
    .unwrap();
}
```
