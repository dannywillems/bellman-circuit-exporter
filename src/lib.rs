/***********************************************************************************/
/*  Copyright (c) 2021 Danny Willems <be.danny.willems@gmail.com>                  */
/*                                                                                 */
/*  Permission is hereby granted, free of charge, to any person obtaining a copy   */
/*  of this software and associated documentation files (the "Software"), to deal  */
/*  in the Software without restriction, including without limitation the rights   */
/*  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell      */
/*  copies of the Software, and to permit persons to whom the Software is          */
/*  furnished to do so, subject to the following conditions:                       */
/*                                                                                 */
/*  The above copyright notice and this permission notice shall be included in all */
/*  copies or substantial portions of the Software.                                */
/*                                                                                 */
/*  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR     */
/*  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,       */
/*  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE    */
/*  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER         */
/*  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,  */
/*  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  */
/*  SOFTWARE.                                                                      */
/***********************************************************************************/

use ff::{Field, PrimeField};
use serde::{Deserialize, Serialize};

use bellman::{Circuit, ConstraintSystem, Index, LinearCombination, SynthesisError, Variable};
use num_bigint::{BigInt, Sign};
use num_traits::identities::One;
use std::fs::File;

// Reference:
// https://docs.zkproof.org/pages/standards/accepted-workshop2/proposal--zk-interop-jr1cs.pdf
#[derive(Serialize, Deserialize)]
struct JSONHeader {
    version: String,
    field_characteristic: String,
    extension_degree: String,
    instances: usize,
    witnesses: usize,
    constraints: usize,
    optimized: bool,
}

#[derive(Serialize, Deserialize)]
struct JSONCircuit {
    r1cs: JSONHeader,
    a: Vec<Vec<(i64, String)>>,
    b: Vec<Vec<(i64, String)>>,
    c: Vec<Vec<(i64, String)>>,
    names: Vec<(i64, String)>,
    evaluations: Vec<(i64, String)>,
}

pub struct RawCircuit<S>
where
    S: PrimeField,
{
    pub num_inputs: usize,
    pub num_aux: usize,
    pub num_constraints: usize,
    pub aux_vars: Vec<(Variable, String)>,
    pub pub_vars: Vec<(Variable, String)>,
    pub aux_eval: Vec<(Variable, Result<S, SynthesisError>)>,
    pub pub_eval: Vec<(Variable, Result<S, SynthesisError>)>,

    pub lc_a: Vec<LinearCombination<S>>,
    pub lc_b: Vec<LinearCombination<S>>,
    pub lc_c: Vec<LinearCombination<S>>,
}

impl<S> ConstraintSystem<S> for RawCircuit<S>
where
    S: PrimeField + Field,
{
    type Root = Self;

    fn alloc<F, A, AR>(&mut self, name: A, val: F) -> Result<Variable, SynthesisError>
    where
        F: FnOnce() -> Result<S, SynthesisError>,
        A: FnOnce() -> AR,
        AR: Into<String>,
    {
        let index = self.num_aux;
        let var = Variable::new_unchecked(Index::Aux(index));
        self.num_aux += 1;
        self.aux_vars.push((var, name().into()));
        self.aux_eval.push((var, val()));

        Ok(var)
    }

    fn alloc_input<F, A, AR>(&mut self, name: A, val: F) -> Result<Variable, SynthesisError>
    where
        F: FnOnce() -> Result<S, SynthesisError>,
        A: FnOnce() -> AR,
        AR: Into<String>,
    {
        let index = self.num_inputs;
        let var = Variable::new_unchecked(Index::Input(index));
        self.num_inputs += 1;
        self.pub_vars.push((var, name().into()));
        self.pub_eval.push((var, val()));

        Ok(var)
    }

    fn enforce<A, AR, LA, LB, LC>(&mut self, _: A, a: LA, b: LB, c: LC)
    where
        A: FnOnce() -> AR,
        AR: Into<String>,
        LA: FnOnce(LinearCombination<S>) -> LinearCombination<S>,
        LB: FnOnce(LinearCombination<S>) -> LinearCombination<S>,
        LC: FnOnce(LinearCombination<S>) -> LinearCombination<S>,
    {
        let a = a(LinearCombination::zero());
        let b = b(LinearCombination::zero());
        let c = c(LinearCombination::zero());

        self.lc_a.push(a);
        self.lc_b.push(b);
        self.lc_c.push(c);

        self.num_constraints += 1;
    }

    fn push_namespace<NR, N>(&mut self, _: N)
    where
        NR: Into<String>,
        N: FnOnce() -> NR,
    {
        // Do nothing; we don't care about namespaces in this context.
    }

    fn pop_namespace(&mut self) {
        // Do nothing; we don't care about namespaces in this context.
    }

    fn get_root(&mut self) -> &mut Self::Root {
        self
    }
}

pub fn export_to_json<C, S>(
    circuit: C,
    filename: String,
    with_evaluation: bool,
) -> Result<(), SynthesisError>
where
    S: PrimeField + Field,
    C: Circuit<S>,
{
    let mut cs = RawCircuit {
        num_inputs: 0,
        num_aux: 0,
        num_constraints: 0,
        aux_vars: vec![],
        pub_vars: vec![],
        aux_eval: vec![],
        pub_eval: vec![],
        lc_a: vec![],
        lc_b: vec![],
        lc_c: vec![],
    };

    // Allocate the "one" input variable
    cs.alloc_input(|| "", || Ok(S::one()))?;

    // Synthesize the circuit.
    circuit.synthesize(&mut cs)?;

    // Input constraints to ensure full density of IC query
    // x * 0 = 0
    // for i in 0..cs.num_inputs {
    //     cs.enforce(
    //         || "",
    //         |lc| lc + Variable::new_unchecked(Index::Input(i)),
    //         |lc| lc,
    //         |lc| lc,
    //     );
    // }

    let order = S::zero() - S::one();
    let order = order.to_repr();
    let order = order.as_ref();
    let order = BigInt::from_bytes_le(Sign::Plus, order) + BigInt::one();
    let r1cs = JSONHeader {
        version: "1.0".to_string(),
        field_characteristic: order.to_str_radix(10),
        extension_degree: "1".to_string(),
        instances: cs.num_inputs,
        witnesses: cs.num_aux,
        constraints: cs.num_constraints,
        optimized: false,
    };

    fn convert_lc<S: PrimeField + Field>(lc: LinearCombination<S>) -> Vec<(i64, String)> {
        lc.as_ref()
            .into_iter()
            .map(|(var, s)| {
                let s = s.to_repr();
                let s = s.as_ref();
                let s = BigInt::from_bytes_le(Sign::Plus, s);
                let s = s.to_str_radix(10);
                match var.get_unchecked() {
                    Index::Input(id) => (-(id as i64), s),
                    Index::Aux(id) => {
                        let new_id = (id + 1) as i64;
                        (new_id, s)
                    }
                }
            })
            .collect()
    }
    let a: Vec<Vec<(i64, String)>> = cs.lc_a.into_iter().map(convert_lc).collect();
    let b: Vec<Vec<(i64, String)>> = cs.lc_b.into_iter().map(convert_lc).collect();
    let c: Vec<Vec<(i64, String)>> = cs.lc_c.into_iter().map(convert_lc).collect();
    let mut names: Vec<(i64, String)> = vec![];
    let mut evaluations: Vec<(i64, String)> = vec![];
    for (v, name) in [cs.pub_vars, cs.aux_vars].concat() {
        match v.get_unchecked() {
            Index::Input(id) => names.push((-(id as i64), name)),
            Index::Aux(id) => names.push(((id + 1) as i64, name)),
        }
    }
    if with_evaluation {
        let aux_eval: Vec<(Variable, S)> = cs
            .aux_eval
            .into_iter()
            .map(|(v, s)| (v, s.unwrap()))
            .collect();
        let pub_eval: Vec<(Variable, S)> = cs
            .pub_eval
            .into_iter()
            .map(|(v, s)| (v, s.unwrap()))
            .collect();
        for (v, s) in [aux_eval, pub_eval].concat() {
            let s = s.to_repr();
            let s = s.as_ref();
            let s = BigInt::from_bytes_le(Sign::Plus, s);
            let s = s.to_str_radix(10);
            match v.get_unchecked() {
                Index::Input(id) => evaluations.push((-(id as i64), s)),
                Index::Aux(id) => evaluations.push(((id + 1) as i64, s)),
            }
        }
    }
    let r1cs = JSONCircuit {
        r1cs,
        a,
        b,
        c,
        names,
        evaluations,
    };
    serde_json::to_writer(&File::create(filename)?, &r1cs).unwrap();

    Ok(())
}
